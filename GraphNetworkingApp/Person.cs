﻿using GraphAlgorithms;

namespace GraphNetworkingApp
{
    public class Person : Node<int>
    {
        public Person(int data) : base (data){}

        public void RemoveNeighbor(Person person1, Person person2)
        {
            person1.RemoveNeighbor(person2);
            person2.RemoveNeighbor(person1);
        }

        public void AddNeighbor(Person person1, Person person2)
        {
            person1.AddNeighbor(person2);
            person2.AddNeighbor(person1);
        }
    }
}