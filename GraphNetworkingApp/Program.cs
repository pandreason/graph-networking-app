﻿using GraphNetworkingApp.GraphUtilities;
using GraphNetworkingApp.TableArrangingServiceModels;

namespace GraphNetworkingApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var numberOfPeople = int.Parse(args[0]);
            var numberOfTables = int.Parse(args[1]);
            var numberOfSeatsAtEachTable = int.Parse(args[3]);

            IGraphUtilities graphUtilities = new GraphUtilities.GraphUtilities();

            var tableArrangingService = new TableArrangingService();

            var nodeList = graphUtilities.PopulateGraph(numberOfPeople);

            var tablesList1 = tableArrangingService.ArrangeFirstRound(numberOfTables, numberOfSeatsAtEachTable, nodeList);
        }
    }
}
