﻿using System;
using System.Collections.Generic;

namespace GraphNetworkingApp.GraphUtilities
{
    public class GraphUtilities : IGraphUtilities
    {
        public List<Person> PopulateGraph(int numberOfPeople)
        {
            var people = new List<Person>();

            for (var i = 0; i < numberOfPeople; i++)
            {
                var newPerson =  new Person(i);

                people.Add(newPerson);
            }

            foreach (var person in people)
            {
                var thisPerson = person;

                foreach (var otherPerson in people)
                {
                    if (thisPerson.Data != otherPerson.Data)
                        thisPerson.AddNeighbor(thisPerson,otherPerson);
                }
            }
            return people;
        }
    }
}
