﻿using System.Collections.Generic;

namespace GraphNetworkingApp.GraphUtilities
{
    public interface IGraphUtilities
    {
        List<Person> PopulateGraph(int numberOfPeople);
    }
}
