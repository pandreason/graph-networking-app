﻿using System.Collections.Generic;

namespace GraphNetworkingApp
{
    public class Table
    {
        public List<Person> PeopleAtTable { get; set; }
    }
}