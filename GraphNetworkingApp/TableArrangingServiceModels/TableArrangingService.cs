﻿using System;
using System.Collections.Generic;
using GraphNetworkingApp.GraphUtilities;

namespace GraphNetworkingApp.TableArrangingServiceModels
{
    public class TableArrangingService : ITableArrangingService
    {
        public List<Table> ArrangeFirstRound(int numberOfTables, int numberOfSeatsAtEachTable, List<Person> people)
        {
            if(people.Count > numberOfTables*numberOfSeatsAtEachTable) throw new InvalidOperationException("There are not enough seats to accomodate everyone!");

            var tablesList = new List<Table>();

            for (var i = 0; i < numberOfTables; i++)
            {
                var newTable = new Table { PeopleAtTable = new List<Person>() };
                for (var j = 1; j < numberOfSeatsAtEachTable + 1; j++)
                {
                    var personToAdd = people.Find(x => x.Data == (j + i * numberOfSeatsAtEachTable));

                    if(personToAdd == null) break;

                    foreach (var person in newTable.PeopleAtTable)
                    {
                        personToAdd.RemoveNeighbor(personToAdd, person);
                    }

                    newTable.PeopleAtTable.Add(personToAdd);
                }

                if (newTable.PeopleAtTable.Count == 0) break;

                tablesList.Add(newTable);
            }

            return tablesList;
        }

    }
}
