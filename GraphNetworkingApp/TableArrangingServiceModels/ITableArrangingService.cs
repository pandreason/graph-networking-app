﻿using System.Collections.Generic;

namespace GraphNetworkingApp.TableArrangingServiceModels
{
    public interface ITableArrangingService
    {
        List<Table> ArrangeFirstRound(int numberOfTables, int numberOfSeatsAtEachTable, List<Person> people);
    }
}
