﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestHelpers
{
    public static class AssertEx
    {
        public static void ThrowsArgumentNullException(Action executable, string argumentName)
        {
            try
            {
                executable();
            }
            catch (Exception ex)
            {
                var argNullException = ex as ArgumentNullException;
                Assert.IsNotNull(argNullException, String.Format("Expected an ArgumentNullException but got {0}", ex.GetType()));
                Assert.AreEqual(argumentName, argNullException.ParamName);
                return;
            }
            // ReSharper disable once RedundantStringFormatCall
            Assert.Fail(String.Format("Expected an ArgumentNullException but no exception was thrown."));
        }

        /// <summary>Check that a statement throws a specific type of exception</summary>
        /// <typeparam name="TException">Exception type inheriting from Exception</typeparam>
        /// <param name="executable">Block that should throw the exception</param>
        public static void Throws<TException>(Action executable) where TException : Exception
        {
            try
            {
                executable();
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex.GetType() == typeof(TException), String.Format("Expected exception of type {0} but got {1}", typeof(TException), ex.GetType()));
                return;
            }
            // ReSharper disable once RedundantStringFormatCall
            Assert.Fail(String.Format("Expected exception of type {0}, but no exception was thrown.", typeof(TException)));
        }

        /// <summary>Check that a statement throws some kind of exception</summary>
        /// <param name="executable">Block that should throw the exception</param>
        /// <param name="message">Failure message to display</param>
        public static void Throws(Action executable, string message = "Expected an exception but none was thrown.")
        {
            try
            {
                executable();
            }
            catch
            {
                Assert.IsTrue(true);
                return;
            }
            Assert.Fail(message);
        }

        /// <summary>Check that a statement does not throw an exception</summary>
        /// <param name="executable">Action to execute</param>
        public static void DoesNotThrow(Action executable)
        {
            try
            {
                executable();
            }
            catch (Exception ex)
            {
                // ReSharper disable once RedundantStringFormatCall
                Assert.Fail(String.Format("Expected no exception, but exception of type {0} was thrown.", ex.GetType()));
            }
        }

        /// <summary>Check that a collection is empty</summary>
        /// <typeparam name="T">Collection type</typeparam>
        /// <param name="collection">The collection</param>
        public static void IsEmpty<T>(ICollection<T> collection)
        {
            Assert.IsTrue(collection.Count == 0, "Empty collection expected, but actual count is " + collection.Count);
        }

        /// <summary>Check that a list/collection is not empty</summary>
        /// <typeparam name="T">Collection type</typeparam>
        /// <param name="collection">Collection in question</param>
        public static void IsNotEmpty<T>(ICollection<T> collection)
        {
            Assert.IsFalse(collection.Count == 0, "Non-empty collection expected, but collection is empty.");
        }
    }
}