﻿using System;
using System.Collections.Generic;
using GraphNetworkingApp;
using GraphNetworkingApp.GraphUtilities;
using GraphNetworkingApp.TableArrangingServiceModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using TestHelpers;

namespace TableArangingService_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_arranging_the_first_table
    {
        private Person person1;
        private Person person2;
        private List<Person> people;
        private TableArrangingService arrangingService;

        [TestInitialize]
        public void TestInititlaize()
        {
            person1 = new Person(1);
            person2 = new Person(2);
            person1.AddNeighbor(person1, person2);

            people = new List<Person> { person1, person2 };

            arrangingService = new TableArrangingService();
        }
        
        [TestMethod]
        public void Then_the_edges_of_the_graph_are_removed_when_people_meet_each_other_at_a_table()
        {
            arrangingService.ArrangeFirstRound(1, 2, people);

            Assert.IsTrue(person1.Neighbors.Count == 0);
            Assert.IsTrue(person2.Neighbors.Count == 0);
        }

        [TestMethod]
        public void And_there_are_more_tables_than_people_then_everyone_is_at_the_same_table()
        {
            var observedNumberOfTables = arrangingService.ArrangeFirstRound(2, 2, people);

            Assert.IsTrue(observedNumberOfTables.Count == 1);
        }

        [TestMethod]
        public void And_there_are_more_people_than_seats_available_then_an_exception_is_thrown()
        {
            AssertEx.Throws<InvalidOperationException>(() =>arrangingService.ArrangeFirstRound(1, 1, people));

        }
    }
    // ReSharper restore InconsistentNaming
}
